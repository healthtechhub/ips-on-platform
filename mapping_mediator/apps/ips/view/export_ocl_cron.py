import requests
import json
import os

# Get the current working directory
current_directory = os.getcwd()

# Construct the path to the 'data' folder within the current working directory
data_folder_path = os.path.join(current_directory, 'data')

# Check if the 'data' folder exists, and create it if it doesn't
if not os.path.exists(data_folder_path):
    os.makedirs(data_folder_path)

def fetch_sources(base_url, token):
    url = f"{base_url}/orgs/<NAMESPACE>/sources"
    headers = {'Authorization': f'Token {token}'}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to fetch sources: {response.status_code}")
        return []

def fetch_mappings_for_source(base_url, source_url, token):
    mappings = []
    page = 1
    while True:
        # Construct the URL with the page query parameter
        url = f"{base_url}{source_url}mappings/?page={page}"
        headers = {'Authorization': f'Token {token}'}
        response = requests.get(url, headers=headers)
        
        if response.status_code == 200:
            data = response.json()
            # Break the loop if no data is returned
            if not data:
                break
            mappings.extend(data)
            page += 1  # Go to the next page
        else:
            print(f"Failed to fetch mappings for {source_url}: {response.status_code}")
            break  # Exit if there's an error response

    return mappings

def save_mappings_to_file(source, mappings):
    file_name = f"{source['short_code']}_mappings.json"
    file_path = os.path.join(data_folder_path, file_name)
    with open(file_path, 'w') as file:
        json.dump(mappings, file)
    print(f"Saved mappings for {source['short_code']} to {file_name}")

def main():
    base_url = "https://api.openconceptlab.org"  # Replace with the actual base URL
    token = "<TOKEN>"  # Replace with your actual token
    sources = fetch_sources(base_url, token)
    
    if not sources:
        print("No sources found. Exiting.")
        return
    
    for source in sources:
        mappings = fetch_mappings_for_source(base_url, source['url'], token)
        save_mappings_to_file(source, mappings)

if __name__ == "__main__":
    main()

from builtins import print

from rest_framework.views import APIView
from rest_framework.response import Response
import uuid
import environ
import requests
import json
env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)
import socket
import sys
#IPAddr = "192.168.100.6"
IPAddr = "openhim-core"

openhimHost = env('OPENHIM_URL')
openhimPort = env('OPENHIM_PORT')

class RegisterPatientIDGeneratorView(APIView):
    def post(self, request):
        try:
            json_data = request.data
            json_data["patientId"] = str(uuid.uuid4())
            json_data["guid"] = str(uuid.uuid4())
            url = "http://"+openhimHost+":"+openhimPort+"/register-response"
            print(url)
            payload = json.dumps(json_data)
            headers = {
                'Authorization': 'bearer {{Token}}',
                'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data=payload)

            print(response)
            return Response(json.loads(response.text))
            #return Response(request.data)  # "uniqueId": "1h288",

        except Exception as e:
            print(e)
            return Response({"Error":"An internal server error occurred", "Exception":str(e)}, status=500)



class FindAndRegisterPatientView(APIView):
    def post(self, request):
        try:
            json_data = request.data
            phn = ""
            goldenId = ""
            status_code = 0
            patientFhirPayload = {}
            parameters = []
            for json_data_resource in json_data.get("entry"):
               resource = json_data_resource.get("resource")
               if resource:
                   if resource.get("resourceType") == "Patient":
                       patientFhirPayload = resource
                       identifiers = resource.get("identifier")
                       self.searchPatientBody(identifiers, parameters)
                       break

            goldenRecords, status_code = self.getGoldenIdByIDs(parameters)

            if goldenRecords.get("entry") and goldenRecords.get("total")==1:
                goldenId = goldenRecords.get("entry").get("resource").get("id")
                print("Patient found for Golden Id: "+goldenId)
            elif goldenRecords.get("total")==0:
                print("Patient not found Registring patient in JeMPI")
                goldenId, status_code = self.registerPatientAndGetGoldenID(goldenId, patientFhirPayload)
                print("Patient registering in JeMPI with GoldenID: "+goldenId)
            else:
                return Response({"Error": "Invalid FHIR Bundle"}, status=403)
            if goldenId and status_code == 201 or status_code == 200:
                new_fhir_json = []
                for fhir_json_data in json_data.get("entry"):
                   fhir_json_resource = fhir_json_data.get("resource")
                   if fhir_json_resource:
                       if fhir_json_resource.get("resourceType") == "Patient":
                           print("Recreating Patient resource for FHIR with Golden ID")
                           patient = {
                               "fullUrl": "Patient/"+goldenId,
                               "resource": {
                                   "resourceType": "Patient",
                                   "id": goldenId,
                               },
                               "request": {
                                   "method": "PUT",
                                   "url": "Patient/"+goldenId
                               }
                           }
                           new_fhir_json.append(patient)
                       if fhir_json_resource.get("resourceType") != "Patient":
                           fhirSubject = fhir_json_resource.get("subject")
                           fhirPatient = fhir_json_resource.get("patient")
                           fhirReceiver = fhir_json_resource.get("receiver")
                           if fhirSubject:
                               fhir_json_data["resource"]["subject"]["reference"] ="Patient/"+goldenId
                           if fhirPatient:
                               fhir_json_data["resource"]["patient"]["reference"] ="Patient/"+goldenId
                           if fhirReceiver:
                               fhir_json_data["resource"]["receiver"][0]["reference"] = "Patient/" + goldenId
                           new_fhir_json.append(fhir_json_data)

                json_data["entry"] = new_fhir_json
                if json_data.get("entry"):
                    url = "http://" + IPAddr + ":5001/fhir"
                    payload = json.dumps(json_data)
                    headers = {
                        'Content-Type': 'application/json'
                    }

                    response = requests.request("POST", url, headers=headers, data=payload)
                    return Response(json.loads(response.text))
            elif status_code == 409:
                return Response({"Patient Already Exists with these ID's"}, status=409)
            else:
                return Response({"Error": "Internal Server Error"}, status=503)

        except Exception as e:
            print(e)
            return Response({"Error":"An internal server error occurred", "Exception":str(e)}, status=500)

    def registerPatientAndGetGoldenID(self, goldenId, patientFhirPayload):
        registerUrl = "http://" + IPAddr + ":5001/fhir/Patient"
        registerPayload = json.dumps(patientFhirPayload)
        headers = {
            'Content-Type': 'application/json'
        }
        newResponse = requests.request("POST", registerUrl, headers=headers, data=registerPayload)
        if newResponse.status_code == 201:
            status_code = 201
            newGoldenRecords = json.loads(newResponse.text)
            if newGoldenRecords.get("id"):
                goldenId = newGoldenRecords.get("id")
            return goldenId, status_code
        elif newResponse.status_code == 409:
            print(newResponse.text)
            status_code = 409
            return newResponse.text, status_code
        else:
            print(newResponse.text)
            status_code = newResponse.status_code
            return newResponse.text, status_code



    def getGoldenIdByIDs(self, parameters):

        serachUrl = "http://" + IPAddr + ":5001/fhir/Patients"
        payload = json.dumps({
            "resourceType": "Parameters",
            "parameters": parameters
        })
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", serachUrl, headers=headers, data=payload)
        if response.status_code == 200:
            goldenRecords = json.loads(response.text)
            status_code = response.status_code
            return goldenRecords, status_code
        else:
            print(response.text)
            status_code = response.status_code
            return response.text, status_code

    def searchPatientBody(self, identifiers, parameters):
        for identifier in identifiers:

            if "phn" in identifier.get("system") and identifier.get("value"):
                phn_data = {
                    "name": "and",
                    "valueCode": "phn",
                    "valueString": identifier.get("value")
                }
                parameters.append(phn_data)

            if "nic" in identifier.get("system") and identifier.get("value"):
                phn_data = {
                    "name": "and",
                    "valueCode": "nic",
                    "valueString": identifier.get("value")
                }
                parameters.append(phn_data)
            if "ppn" in identifier.get("system") and identifier.get("value"):
                phn_data = {
                    "name": "and",
                    "valueCode": "ppn",
                    "valueString": identifier.get("value")
                }
                parameters.append(phn_data)
            if "scn" in identifier.get("system") and identifier.get("value"):
                phn_data = {
                    "name": "and",
                    "valueCode": "scn",
                    "valueString": identifier.get("value")
                }
                parameters.append(phn_data)
            if "dl" in identifier.get("system") and identifier.get("value"):
                phn_data = {
                    "name": "and",
                    "valueCode": "dl",
                    "valueString": identifier.get("value")
                }
                parameters.append(phn_data)

